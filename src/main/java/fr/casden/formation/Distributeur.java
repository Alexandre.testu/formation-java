package fr.casden.formation;

import java.util.Hashtable;
import java.util.Map;

public class Distributeur {

    private Boisson boisson;
    private double monnaie_total_versee = 0d;
    private double ecart = 0d;
    private double piece_introduite = 0d;
    private Map<String, Boisson> table;

    public Distributeur(Map<String, Boisson> table) {
        this.table = table;
    }

    public void choixBoisson(String boisson) {
        // on controle que la boisson existe
        if(table.containsKey(boisson)) {
             this.boisson = table.get(boisson);
             System.out.println("Choix enregistré. Merci d'introduire " + this.boisson.getTarif() + "EUR");
        } else {
             System.out.println("La boisson choisie n'existe pas.");
        }
    }

    public void introduirePaiement(double monnaie) {
        this.monnaie_total_versee = this.monnaie_total_versee + monnaie;
        this.piece_introduite = monnaie;
        System.out.println("Pièce de "+ monnaie + "EUR introduite");
    }

    public void rendlaMonnaie() {
        if(this.monnaie_total_versee + this.piece_introduite < this.boisson.getTarif()) {
            ecart = this.boisson.getTarif() - this.monnaie_total_versee;
            System.out.println("Il manque "+ ecart + " EUR. Merci de verser ce montant");
        } else {
            ecart = this.boisson.getTarif() - this.monnaie_total_versee;
            if(ecart == 0d) {
                System.out.println("Montant versé OK");
                Distribuer();
            } else {
                System.out.println("Montant versé de " + this.monnaie_total_versee + "EUR. Monnaie à rendre de " + -ecart + "EUR");
            } 
        }
    }

    private void Distribuer() {
        
        Utils.println("Distribution en cours de la boisson :" + boisson.getLibelle());
        
        //return "Distribution terminée";
    }

}