package fr.casden.formation;

import java.util.HashMap;
import java.util.Hashtable;

/*import fr.casden.formation.model.Utilisateur;


public class App 
{
    public static void main( String[] args )
    {
        for(int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }

        Utilisateur user = new Utilisateur("Testu", "Alexandre", 39);

        //user.setAge(user.getAge()+1);
        user.vieillir(80);

        System.out.printf("Hello %s %s %d ans !\n", user.getPrenom(), user.getNom(), user.getAge());

        //System.out.println("Hello World!");
    }
}

*/


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        HashMap<String, Boisson> table = new HashMap<String, Boisson>();
        Distributeur d1 = new Distributeur(table);
        table.put("Café", new Boisson("Café", 4d));
        table.put("Eau", new Boisson("Eau", 1.5d));

     

        d1.choixBoisson("Café");
        d1.introduirePaiement(4d);
        d1.rendlaMonnaie();

    }
}
